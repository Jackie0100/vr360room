﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZombieSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject[] spawnableEnemies;


    int waveCounter;
	
	// Update is called once per frame
	void Update ()
    {
        if (this.transform.childCount == 0)
        {
            waveCounter++;

            for (int i = 0; i < waveCounter; i++)
            {
                Vector2 spawnpos = (Random.insideUnitCircle.normalized * 100);
                GameObject go = (GameObject)GameObject.Instantiate(spawnableEnemies[Random.Range(0, spawnableEnemies.Length)], new Vector3(spawnpos.x, 2, spawnpos.y), Quaternion.identity);
                go.transform.parent = this.transform;
            }
        }
	}

    public void ResetComponent()
    {
        waveCounter = 0;
    }
}
