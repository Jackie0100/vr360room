﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PlayerCamp : MonoBehaviour
{
    [SerializeField]
    GameObject zombieSpawner;
    [SerializeField]
    GameObject gameOverScreen;
    [SerializeField]
    public GameObject[] Weapons;


    public List<Player> Players { get; set; }
    public bool IsAllPlayerReady
    {
        get
        {
            if (Players.Count > 0)
            {
                return Players.All(p => p.IsReady);
            }
            else
            {
                return false;
            }
        }
    }

    public bool IsAllPlayersDead
    {
        get { return Players.Count != 0 && Players.All(p => p.Health <= 0); }
    }

    // Use this for initialization
    void Start ()
    {
        Players = new List<Player>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (IsAllPlayerReady && !zombieSpawner.GetComponent<ZombieSpawner>().enabled)
        {
            zombieSpawner.GetComponent<ZombieSpawner>().enabled = true;
        }
        if (IsAllPlayersDead)
        {
            zombieSpawner.GetComponent<ZombieSpawner>().ResetComponent();
            zombieSpawner.GetComponent<ZombieSpawner>().enabled = false;
            gameOverScreen.SetActive(true);
            while (Players.Count != 0)
            {
                Players[0].CrossHairObject.DestroyCrossHairSafely();
                GameObject.Destroy(Players[0].gameObject);
                Players.RemoveAt(0);
            }
        }
	}

    public void DamagePlayer(Zombie zombie)
    {
        Players[Random.Range(0, Players.Count)].Health -= zombie.damage;
    }
}
