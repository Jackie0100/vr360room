﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

public class MenuPanel : MonoBehaviour
{
    [SerializeField]
    int joystickNumber = 1;
    [SerializeField]
    int joystickAxisY = 6;
    [SerializeField]
    float indexChangeDelay = 0.25f;

    public PlayerCamp playerCamp;

    float indexChangeTimer = 0;

    public SelectionPanel[] subPanels;
    int index = 0;

    public int JoystickNumber
    {
        get
        {
            return joystickNumber;
        }
        set
        {
            foreach (SelectionPanel sp in subPanels)
            {
                sp.joystickNumber = value;
            }
            joystickNumber = value;
        }
    }

    int Index
    {
        get
        {
            return index;
        }
        set
        {
            subPanels[index].GetComponent<Image>().color = new Color(1, 1, 1, 0);
            subPanels[index].enabled = false;
            if (value < 0)
            {
                index = subPanels.Length - 1;
            }
            else if (value >= subPanels.Length)
            {
                index = 0;
            }
            else
            {
                index = value;
            }
            subPanels[index].GetComponent<Image>().color = new Color(1, 1, 1, 0.25f);
            subPanels[index].enabled = true;
        }
    }    


    // Use this for initialization
    void Awake ()
    {
        subPanels = this.GetComponentsInChildren<SelectionPanel>();
        foreach (SelectionPanel go in subPanels)
        {
            go.enabled = false;
        }
        Index = index;
    }

    // Update is called once per frame
    void Update ()
    {
        indexChangeTimer += Time.deltaTime;
        if (indexChangeTimer < indexChangeDelay)
        {
            return;
        }
        else
        {
            indexChangeTimer = 0;
        }

        if (Input.GetAxis("Joystick" + joystickNumber + "Axis" + joystickAxisY) <= -0.5f)
        {
            Index++;
        }
        else if (Input.GetAxis("Joystick" + joystickNumber + "Axis" + joystickAxisY) >= 0.5f)
        {
            Index--;
        }

        if (playerCamp.IsAllPlayerReady)
        {
            Destroy(this.gameObject);
        }
    }
}
