﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameoverScript : MonoBehaviour
{
    [SerializeField]
    GameObject zombieSpawner;
    [SerializeField]
    GameObject zombieGraveyard;
    [SerializeField]
    GameObject StartGameText;


    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.JoystickButton7))
        {
            while (zombieSpawner.transform.childCount > 0)
            {
                GameObject.Destroy(zombieSpawner.transform.GetChild(0).gameObject);
                zombieSpawner.transform.GetChild(0).parent = null;
            }
            while (zombieGraveyard.transform.childCount > 0)
            {
                GameObject.Destroy(zombieGraveyard.transform.GetChild(0).gameObject);
                zombieGraveyard.transform.GetChild(0).parent = null;
            }
            StartGameText.SetActive(true);
            StartGameText.GetComponent<StartGameText>().ResetComponent();
            this.gameObject.SetActive(false);
        }
    }

    void OnEnable()
    {
        this.transform.GetChild(8).GetComponent<Text>().text = "You killed " + zombieGraveyard.transform.childCount + " Zombies!";
        this.transform.GetChild(9).GetComponent<Text>().text = "You killed " + zombieGraveyard.transform.childCount + " Zombies!";
        this.transform.GetChild(10).GetComponent<Text>().text = "You killed " + zombieGraveyard.transform.childCount + " Zombies!";
        this.transform.GetChild(11).GetComponent<Text>().text = "You killed " + zombieGraveyard.transform.childCount + " Zombies!";
    }
}
