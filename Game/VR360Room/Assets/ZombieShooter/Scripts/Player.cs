﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

//TODO: ScoreBoard
public class Player : MonoBehaviour
{
    public int playerJoystick;
    public int playerNumber;
    [SerializeField]
    Crosshair crossHairObject;
    public float Health { get; set; }
    public bool IsReady { get; set; }

    public Weapon PrimaryWeapon;

    public Weapon SecondaryWeapon;
    public Weapon ExtraWeapon;

    public Crosshair CrossHairObject
    {
        get
        {
            return crossHairObject;
        }
        set
        {
            crossHairObject = value;
            crossHairObject.player = this;
        }
    }

    void Start()
    {
        Health = 100;
    }

    void Update()
    {
        transform.rotation = Quaternion.Euler(0,  crossHairObject.transform.position.x * 360.0f / (float)Screen.width - 110, 0);
    }

    public void ChangeReadyState(string state)
    {
        if (state == "Yes")
        {
            IsReady = true;
        }
        else
        {
            IsReady = false;
        }
    }

    public void ChangeSensitivity(string sensitivity)
    {
        CrossHairObject.SensitivityX = int.Parse(sensitivity);
        CrossHairObject.SensitivityY = int.Parse(sensitivity);
    }

    public void ChangeInverseY(string inverse)
    {
        if (inverse == "On")
        {
            CrossHairObject.InvertY = true;
        }
        else
        {
            CrossHairObject.InvertY = false;
        }
    }

    public void ChangeColor(string color)
    {
        if (color == "Red")
        {
            CrossHairObject.GetComponent<Image>().color = new Color(1, 0, 0, 1);
        }
        else if (color == "Green")
        {
            CrossHairObject.GetComponent<Image>().color = new Color(0, 0.5f, 0, 1);
        }
        else if (color == "Blue")
        {
            CrossHairObject.GetComponent<Image>().color = new Color(0, 0, 1, 1);
        }
        else if (color == "Yellow")
        {
            CrossHairObject.GetComponent<Image>().color = new Color(1, 1, 0, 1);
        }
        else if (color == "Purple")
        {
            CrossHairObject.GetComponent<Image>().color = new Color(0.5f, 0, 0.5f, 1);
        }
        else if (color == "Orange")
        {
            CrossHairObject.GetComponent<Image>().color = new Color(1, 0.66f, 0, 1);
        }
        else if (color == "Cyan")
        {
            CrossHairObject.GetComponent<Image>().color = new Color(0, 1, 1, 1);
        }
        else if (color == "White")
        {
            CrossHairObject.GetComponent<Image>().color = new Color(1, 1, 1, 1);
        }
        else if (color == "Coral")
        {
            CrossHairObject.GetComponent<Image>().color = new Color(1, 0.5f, 0.25f, 1);
        }
        else if (color == "Lime")
        {
            CrossHairObject.GetComponent<Image>().color = new Color(0, 1, 0, 1);
        }
        else if (color == "Magenta")
        {
            CrossHairObject.GetComponent<Image>().color = new Color(1, 0, 1, 1);
        }
        else if (color == "Khaki")
        {
            CrossHairObject.GetComponent<Image>().color = new Color(0.95f, 0.95f, 0.55f, 1);
        }
        else if (color == "Violet")
        {
            CrossHairObject.GetComponent<Image>().color = new Color(0.95f, 0.5f, 0.95f, 1);
        }
        else if (color == "Sky")
        {
            CrossHairObject.GetComponent<Image>().color = new Color(0.55f, 0.8f, 0.95f, 1);
        }
        else if (color == "Olive")
        {
            CrossHairObject.GetComponent<Image>().color = new Color(0.5f, 0.5f, 0, 1);
        }
    }

    public void ChangePrimaryWeapon(string weapon)
    {
        if (PrimaryWeapon != null)
        {
            GameObject.Destroy(PrimaryWeapon.gameObject);
        }
        if (weapon == "M4A1 Assult")
        {
            GameObject go = GameObject.Instantiate(this.transform.parent.GetComponent<PlayerCamp>().Weapons[0]);
            go.transform.SetParent(this.transform, false);
            PrimaryWeapon = go.GetComponent<Weapon>();
            PrimaryWeapon.JoystickNumberOwner = playerJoystick;
        }
        else if (weapon == "AK47 Riffle")
        {
            GameObject go = GameObject.Instantiate(this.transform.parent.GetComponent<PlayerCamp>().Weapons[1]);
            go.transform.SetParent(this.transform, false);
            PrimaryWeapon = go.GetComponent<Weapon>();
            PrimaryWeapon.JoystickNumberOwner = playerJoystick;
        }
        else if (weapon == "Shotgun")
        {
            GameObject go = GameObject.Instantiate(this.transform.parent.GetComponent<PlayerCamp>().Weapons[5]);
            go.transform.SetParent(this.transform, false);
            PrimaryWeapon = go.GetComponent<Weapon>();
            PrimaryWeapon.JoystickNumberOwner = playerJoystick;
        }
        else if (weapon == "UMP-45")
        {
            GameObject go = GameObject.Instantiate(this.transform.parent.GetComponent<PlayerCamp>().Weapons[4]);
            go.transform.SetParent(this.transform, false);
            PrimaryWeapon = go.GetComponent<Weapon>();
            PrimaryWeapon.JoystickNumberOwner = playerJoystick;
        }
        else if (weapon == "SMG")
        {
            GameObject go = GameObject.Instantiate(this.transform.parent.GetComponent<PlayerCamp>().Weapons[3]);
            go.transform.SetParent(this.transform, false);
            PrimaryWeapon = go.GetComponent<Weapon>();
            PrimaryWeapon.JoystickNumberOwner = playerJoystick;
        }
        else if (weapon == "Riffle")
        {
            GameObject go = GameObject.Instantiate(this.transform.parent.GetComponent<PlayerCamp>().Weapons[6]);
            go.transform.SetParent(this.transform, false);
            PrimaryWeapon = go.GetComponent<Weapon>();
            PrimaryWeapon.JoystickNumberOwner = playerJoystick;
        }
        else if (weapon == "Revolver")
        {
            GameObject go = GameObject.Instantiate(this.transform.parent.GetComponent<PlayerCamp>().Weapons[2]);
            go.transform.SetParent(this.transform, false);
            PrimaryWeapon = go.GetComponent<Weapon>();
            PrimaryWeapon.JoystickNumberOwner = playerJoystick;
        }
        else if (weapon == "Crossbow")
        {
            GameObject go = GameObject.Instantiate(this.transform.parent.GetComponent<PlayerCamp>().Weapons[7]);
            go.transform.SetParent(this.transform, false);
            PrimaryWeapon = go.GetComponent<Weapon>();
            PrimaryWeapon.JoystickNumberOwner = playerJoystick;
        }
    }

    public void ChangeSecondaryWeapon(string weapon)
    {
        //TODO: Create more weapons

    }
}