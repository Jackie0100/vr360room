﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Linq;
using System.Collections;

[System.Serializable]
public class JoystickStartEvent : UnityEvent<int> { }

public class StartGameText : MonoBehaviour
{
    [SerializeField]
    GameObject crossHair;
    [SerializeField]
    GameObject menuPanel;
    [SerializeField]
    GameObject playerCamp;
    [SerializeField]
    UnityEvent StartEvent;
    [SerializeField]
    JoystickStartEvent JoystickNumberStartEvent;

    bool[] currentPlayers;
    int maxPlayers = 4;

    public int PlayerCount
    {
        get
        {
            return currentPlayers.Count(b => b == true);
        }
    }

    // Use this for initialization
    void Start ()
    {
        currentPlayers = new bool[maxPlayers];
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Joystick1Button7) && !currentPlayers[0])
        {
            SetupPlayer(0);
        }
        else if (Input.GetKeyDown(KeyCode.Joystick2Button7) && !currentPlayers[1])
        {
            SetupPlayer(1);
        }
        else if (Input.GetKeyDown(KeyCode.Joystick3Button7) && !currentPlayers[2])
        {
            SetupPlayer(2);
        }
        else if (Input.GetKeyDown(KeyCode.Joystick4Button7) && !currentPlayers[3])
        {
            SetupPlayer(3);
        }

        if (playerCamp.GetComponent<PlayerCamp>().IsAllPlayerReady)
        {
            this.gameObject.SetActive(false);
        }
    }

    public void ResetComponent()
    {
        currentPlayers = new bool[maxPlayers];
        this.GetComponent<RectTransform>().anchorMin = new Vector2((1.0f / (float)maxPlayers) * (PlayerCount), 0);
        this.GetComponent<RectTransform>().anchorMax = new Vector2(((1.0f / (float)maxPlayers) * (PlayerCount) + (1.0f / (float)maxPlayers)), 0.2f);
    }

    void SetupPlayer(int index)
    {
        currentPlayers[index] = true;
        JoystickNumberStartEvent.Invoke(index + 1);
        StartEvent.Invoke();
        GameObject menupanel = GameObject.Instantiate(menuPanel);
        menupanel.transform.SetParent(this.transform.root, false);
        menupanel.GetComponent<MenuPanel>().JoystickNumber = index + 1;
        menupanel.GetComponent<RectTransform>().anchorMin = new Vector2((1.0f / (float)maxPlayers) * (PlayerCount - 1), 0);
        menupanel.GetComponent<RectTransform>().anchorMax = new Vector2(((1.0f / (float)maxPlayers) * (PlayerCount - 1)) + (1.0f / (float)maxPlayers), 1f);
        
        GameObject crosshair = GameObject.Instantiate(crossHair);
        crosshair.GetComponent<Crosshair>().JoystickNumber = index + 1;
        crosshair.transform.SetParent(this.transform.root, false);

        GameObject playerobj = new GameObject("Player " + PlayerCount);
        playerobj.transform.SetParent(playerCamp.transform);
        Player player = playerobj.AddComponent<Player>();
        player.CrossHairObject = crosshair.GetComponent<Crosshair>();
        playerCamp.GetComponent<PlayerCamp>().Players.Add(player);
        player.CrossHairObject.player = player;
        player.playerJoystick = index + 1;
        player.playerNumber = PlayerCount;

        menupanel.GetComponent<MenuPanel>().playerCamp = playerCamp.GetComponent<PlayerCamp>();
        menupanel.GetComponent<MenuPanel>().subPanels[0].onTextChanged.AddListener(player.ChangePrimaryWeapon);
        menupanel.GetComponent<MenuPanel>().subPanels[1].onTextChanged.AddListener(player.ChangeSecondaryWeapon);
        menupanel.GetComponent<MenuPanel>().subPanels[2].onTextChanged.AddListener(player.ChangeSensitivity);
        menupanel.GetComponent<MenuPanel>().subPanels[3].onTextChanged.AddListener(player.ChangeInverseY);
        menupanel.GetComponent<MenuPanel>().subPanels[4].onTextChanged.AddListener(player.ChangeColor);
        menupanel.GetComponent<MenuPanel>().subPanels[5].onTextChanged.AddListener(player.ChangeReadyState);
        player.ChangePrimaryWeapon("M4A1 Assult");
        player.ChangeSecondaryWeapon("Baseball Bat");

        if (PlayerCount <= maxPlayers - 1)
        {
            this.GetComponent<RectTransform>().anchorMin = new Vector2((1.0f / (float)maxPlayers) * (PlayerCount), 0);
            this.GetComponent<RectTransform>().anchorMax = new Vector2(((1.0f / (float)maxPlayers) * (PlayerCount) + (1.0f / (float)maxPlayers)), 0.2f);
        }
        else
        {
            this.gameObject.GetComponent<Text>().enabled = false;
        }
    }
}
