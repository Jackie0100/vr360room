﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Gui360))]
public class Crosshair : MonoBehaviour
{
    [SerializeField]
    public float SensitivityX = 1;
    [SerializeField]
    public float SensitivityY = 1;
    [SerializeField]
    public int JoystickNumber = 1;
    [SerializeField]
    int movementAxisX = 1;
    [SerializeField]
    int movementAxisY = 2;
    [SerializeField]
    public bool InvertY = false;

    [SerializeField]
    bool useBorderX = false;
    [SerializeField]
    bool useBorderY = false;
    [SerializeField]
    bool useMouse = false;

    Camera360 surroundCamera;
    public Player player;

    // Use this for initialization
    void Start ()
    {
        player.CrossHairObject = this;
        surroundCamera = GameObject.Find("360Camera").GetComponent<Camera360>();
	}
    public float vec;
	// Update is called once per frame
	void Update ()
    {
         Vector3 movement = new Vector3(Input.GetAxis("Joystick" + JoystickNumber + "Axis" + movementAxisX) * SensitivityX, 
            (InvertY ? Input.GetAxis("Joystick" + JoystickNumber + "Axis" + movementAxisY) * SensitivityY : -Input.GetAxis("Joystick" + JoystickNumber + "Axis" + movementAxisY) * SensitivityY), 0);
        
        if (useMouse)
        {
            movement = new Vector3(Input.GetAxis("Mouse X") * SensitivityX, (InvertY ? -Input.GetAxis("Mouse Y") * SensitivityY : Input.GetAxis("Mouse Y") * SensitivityY), 0);
            if (Input.GetKey(KeyCode.Mouse0))
            {
                player.PrimaryWeapon.Shoot(surroundCamera.transform.position, surroundCamera.ScreenToWorldPoint(new Vector3(
                    this.transform.position.x, this.transform.position.y - (this.GetComponent<Gui360>().sizeY * 1.5f), 10)));
            }
        }
        /*
        Debug.DrawRay(surroundCamera.transform.position, surroundCamera.ScreenToWorldPoint(new Vector3(
        this.transform.position.x, this.transform.position.y, 10)), Color.white);

        Debug.DrawRay(surroundCamera.transform.position, surroundCamera.ScreenToWorldPoint(new Vector3(
                this.transform.position.x - (this.GetComponent<Gui360>().sizeX),
                this.transform.position.y - (this.GetComponent<Gui360>().sizeY), 10)), Color.red);

        Debug.DrawRay(surroundCamera.transform.position, surroundCamera.ScreenToWorldPoint(new Vector3(
                this.transform.position.x,
                this.transform.position.y - (this.GetComponent<Gui360>().sizeY * 1.5f), 10)), Color.green);
                */
        if (Input.GetKey((KeyCode)Enum.Parse(typeof(KeyCode), "Joystick" + JoystickNumber + "Button4")))
        {
            movement *= 2f;
        }
        else if (Input.GetKey((KeyCode)Enum.Parse(typeof(KeyCode), "Joystick" + JoystickNumber + "Button5")))
        {
            movement /= 4f;
        }

        if (Input.GetKey((KeyCode)Enum.Parse(typeof(KeyCode), "Joystick" + JoystickNumber + "Button0")))
        {
            player.PrimaryWeapon.Shoot(surroundCamera.transform.position, surroundCamera.ScreenToWorldPoint(new Vector3(
                this.transform.position.x, this.transform.position.y - (this.GetComponent<Gui360>().sizeY * 1.5f), 10)));
        }

        if (useBorderX)
        {
            if (this.transform.position.x + movement.x < 0)
            {
                movement = new Vector3(-this.transform.position.x, movement.y, 0);
            }
            else if (this.transform.position.x + movement.x > this.GetComponent<Gui360>().GetCanvasWidth() - this.GetComponent<RectTransform>().sizeDelta.x)
            {
                movement = new Vector3((this.GetComponent<Gui360>().GetCanvasWidth() - this.GetComponent<RectTransform>().sizeDelta.x) - this.transform.position.x, movement.y, 0);
            }
        }
        if (useBorderY)
        {
            if (this.transform.position.y + movement.y < 0)
            {
                movement = new Vector3(movement.x, -this.transform.position.y, 0);
            }
            else if (this.transform.position.y + movement.y > this.GetComponent<Gui360>().GetCanvasHeight() - (this.GetComponent<RectTransform>().sizeDelta.y / 2.0f))
            {
                movement = new Vector3(movement.x, (this.GetComponent<Gui360>().GetCanvasHeight() - (this.GetComponent<RectTransform>().sizeDelta.y / 2.0f)) - this.transform.position.y, 0);
            }
        }
        this.transform.position += movement;
    }

    public void DestroyCrossHairSafely()
    {
        if(this.GetComponent<Gui360>().spawnedVerticalGameobject != null)
        {
            GameObject.Destroy(this.GetComponent<Gui360>().spawnedVerticalGameobject);
        }
        if (this.GetComponent<Gui360>().spawnedHorizontalGameobject != null)
        {
            GameObject.Destroy(this.GetComponent<Gui360>().spawnedHorizontalGameobject);
        }
        GameObject.Destroy(this.gameObject);
    }

    void OnDestroy()
    {
        if (this.GetComponent<Gui360>().spawnedHorizontalGameobject != null)
        {
            player.CrossHairObject = this.GetComponent<Gui360>().spawnedHorizontalGameobject.GetComponent<Crosshair>();
        }
    }
}
