﻿using UnityEngine;
using System.Collections;

public class Zombie : MonoBehaviour
{
    float speed = 5;
    float health = 100;

    public float damage = 5;
    public float specialDamage = 20;

    [SerializeField]
    PlayerCamp playerCamp;
    [SerializeField]
    GameObject player;
    [SerializeField]
    AudioClip[] attack;
    [SerializeField]
    AudioClip[] hunt;
    [SerializeField]
    AudioClip[] die;
    [SerializeField]
    AudioClip[] wound;

    AudioSource audioPlayer;

    public bool IsDead
    {
        get { return (health <= 0); }
    }

    // Use this for initialization
    void Start ()
    {
        if (this.gameObject.GetComponent<AudioSource>() == null)
        {
            audioPlayer = this.gameObject.AddComponent<AudioSource>();
        }
        else
        {
            audioPlayer = this.gameObject.GetComponent<AudioSource>();
        }
        player = GameObject.Find("360Camera");
        this.transform.rotation = Quaternion.LookRotation((new Vector3(player.transform.position.x, 0, player.transform.position.z) - new Vector3(this.transform.position.x, 0, this.transform.position.z)).normalized);
        playerCamp = GameObject.Find("PlayerCamp").GetComponent<PlayerCamp>();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerCamp.IsAllPlayersDead || playerCamp.Players.Count == 0)
        {
            this.GetComponent<Animator>().SetBool("idle0ToRun", false);
            this.GetComponent<Animator>().SetBool("idle0ToWound", false);
            this.GetComponent<Animator>().SetBool("runToIdle0", false);
            this.GetComponent<Animator>().SetBool("idle0ToAttack0", false);
            return;
        }
        if (IsDead)
            return;

        this.transform.rotation = Quaternion.LookRotation((new Vector3(player.transform.position.x, 0, player.transform.position.z) - new Vector3(this.transform.position.x, 0, this.transform.position.z)).normalized);

        if (Vector3.Distance(player.transform.position, this.transform.position) > 3)
        {
            this.GetComponent<Animator>().SetBool("idle0ToRun", true);
            this.GetComponent<Animator>().SetBool("idle0ToWound", false);

            this.transform.Translate(Vector3.forward * Time.deltaTime * speed);

            if (!audioPlayer.isPlaying)
            {
                audioPlayer.clip = hunt[Random.Range(0, hunt.Length)];
                audioPlayer.Play();
            }
        }
        else
        {
            this.GetComponent<Animator>().SetBool("idle0ToRun", false);
            this.GetComponent<Animator>().SetBool("idle0ToWound", false);
            this.GetComponent<Animator>().SetBool("runToIdle0", true);
            this.GetComponent<Animator>().SetBool("idle0ToAttack0", true);

            if (!audioPlayer.isPlaying)
            {
                playerCamp.DamagePlayer(this);
                audioPlayer.clip = attack[Random.Range(0, attack.Length)];
                audioPlayer.Play();
            }
        }
    }

    public void DealDamage(float weapondamage)
    {
        health -= weapondamage;
        audioPlayer.Stop();
        audioPlayer.clip = wound[Random.Range(0, wound.Length)];
        audioPlayer.Play();

        if (IsDead)
        {
            this.GetComponent<Animator>().SetBool("idle0ToRun", false);
            this.GetComponent<Animator>().SetBool("runToIdle0", true);
            this.GetComponent<Animator>().SetBool("idle0ToAttack0", false);
            this.GetComponent<Animator>().SetBool("idle0ToDeath", true);
            this.transform.parent = GameObject.Find("ZombieGraveyard").transform;
            DisablePhysics(this.gameObject);
            this.GetComponent<Rigidbody>().isKinematic = true;

            audioPlayer.Stop();
            audioPlayer.clip = die[Random.Range(0, die.Length)];
            audioPlayer.Play();
        }
        else
        {
            this.GetComponent<Animator>().SetBool("idle0ToRun", false);
            this.GetComponent<Animator>().SetBool("runToIdle0", false);
            this.GetComponent<Animator>().SetBool("idle0ToAttack0", false);
            this.GetComponent<Animator>().SetBool("idle0ToWound", true);
        }
    }

    void DisablePhysics(GameObject go)
    {
        if (go.GetComponent<Collider>() != null)
        {
            go.GetComponent<Collider>().enabled = false;
        }
        foreach(Transform t in go.transform)
        {
            DisablePhysics(t.gameObject);
        }
    }
}