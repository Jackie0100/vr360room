﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

[System.Serializable]
public class StringChangeEvent : UnityEvent<string> { }
public class SelectionPanel : MonoBehaviour
{
    [SerializeField]
    Text selectionText;
    [SerializeField]
    Image leftImage;
    [SerializeField]
    Image rightImage;
    [SerializeField]
    public int joystickNumber = 1;
    [SerializeField]
    int joystickAxisX = 6;
    [SerializeField]
    string[] selectionOptions;
    [SerializeField]
    bool useNumbersOnly = false;
    [SerializeField]
    int minValue = 1;
    [SerializeField]
    int maxValue = 10;
    [SerializeField]
    int index = 0;
    [SerializeField]
    public StringChangeEvent onTextChanged;

    int Index
    {
        get
        {
            return index;
        }
        set
        {
            if (useNumbersOnly)
            {
                if (value > maxValue)
                {
                    index = maxValue;
                }
                else if (value < minValue)
                {
                    index = minValue;
                }
                else
                {
                    index = value;
                }
                selectionText.text = Index.ToString();
            }
            else
            {
                if (value >= selectionOptions.Length)
                {
                    index = selectionOptions.Length - 1;
                }
                else if (value < 0)
                {
                    index = 0;
                }
                else
                {
                    index = value;
                }
                selectionText.text = selectionOptions[Index];
            }
            onTextChanged.Invoke(selectionText.text);
        }
    }

    // Use this for initialization
    void Awake ()
    {
        Index = index;
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (Input.GetAxis("Joystick" + joystickNumber + "Axis" + joystickAxisX) <= -0.5f)
        {
            if (useNumbersOnly)
            {
                Index--;

            }
            else if (selectionOptions.Length != 0)
            {
                Index--;
            }
        }
        else if (Input.GetAxis("Joystick" + joystickNumber + "Axis" + joystickAxisX) >= 0.5f)
        {
            if (useNumbersOnly)
            {
                Index++;

            }
            else if (selectionOptions.Length != 0)
            {
                Index++;
            }
        }
	}

    string ParseText()
    {
        if (selectionText.text.ToLower() == "off")
        {
            return "false";
        }
        else if (selectionText.text.ToLower() == "on")
        {
            return "true";
        }
        else
        {
            return selectionText.text;
        }
    }
}
