﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Shotgun : Weapon
{
    [SerializeField]
    int fragtures = 8;
    public override void Shoot(Vector3 origin, Vector3 direction)
    {
        if (!IsAutomatic && !IsTriggerReleased)
        {
            return;
        }
        if (ShotResetTimer < Time.time - ShotTimer)
        {
            for (int i = 0; i < fragtures; i++)
            {
                IsTriggerReleased = true;
                ShotTimer = 0;
                base.Shoot(origin, direction);
            }
        }
    }
}