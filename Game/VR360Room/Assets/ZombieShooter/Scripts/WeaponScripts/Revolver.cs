﻿using UnityEngine;

public class Revolver : Weapon
{
    public override void Shoot(Vector3 origin, Vector3 direction)
    {
        if (!IsAutomatic && !IsTriggerReleased)
        {
            return;
        }

        IsTriggerReleased = false;

        if (ShotResetTimer < Time.time - ShotTimer)
        {
            ShotTimer = Time.time;
            AudioPlayer.Stop();
            AudioPlayer.Play();
            RaycastHit hit;
            Ray ray = new Ray(origin, direction + new Vector3(UnityEngine.Random.Range(-Spread, Spread), UnityEngine.Random.Range(-Spread, Spread), UnityEngine.Random.Range(-Spread, Spread)));

            if (Physics.Raycast(ray, out hit, Range))
            {
                if (hit.collider.tag == "Zombie")
                {
                    hit.transform.GetComponent<Zombie>().DealDamage(CalculateDamage(ParseBodypart(hit.collider.name)));
                }
            }
        }
    }
    public override float CalculateDamage(Bodypart bodypart)
    {
        switch (bodypart)
        {
            case Bodypart.Chest:
                return Damage * 2.5f;
            case Bodypart.Head:
                return Damage * 10f;
            default:
                return base.CalculateDamage(bodypart);
        }
    }
}