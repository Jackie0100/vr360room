﻿using System;
using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour
{
    [HideInInspector]
    public int JoystickNumberOwner;
    [SerializeField]
    protected float Range;
    [SerializeField]
    protected AudioClip Fire;
    [SerializeField]
    protected AudioClip ClipIn;
    [SerializeField]
    protected AudioClip ClipOut;
    [SerializeField]
    protected AudioClip Bolt;
    [SerializeField]
    protected float Damage;
    [SerializeField]
    protected float ShotsPerSecond;

    protected float ShotTimer;
    [SerializeField]
    protected float Spread;

    protected float ShotResetTimer { get { return 1.0f / ShotsPerSecond; } }
    [SerializeField]
    protected bool IsAutomatic;
    protected bool IsTriggerReleased;

    protected AudioSource AudioPlayer;

    public virtual void Start()
    {
        AudioPlayer = this.gameObject.AddComponent<AudioSource>();
        AudioPlayer.spatialBlend = 1;
        AudioPlayer.clip = Fire;
        AudioPlayer.playOnAwake = false;
        AudioPlayer.loop = false;
        AudioPlayer.volume = 0.8f;
    }

    public virtual void Update()
    {
        if (Input.GetKeyUp((KeyCode)Enum.Parse(typeof(KeyCode), "Joystick" + JoystickNumberOwner + "Button0")))
        {
            IsTriggerReleased = true;
        }
    }

    public virtual void Shoot(Vector3 origin, Vector3 direction)
    {
        if (!IsAutomatic && !IsTriggerReleased)
        {
            return;
        }

        IsTriggerReleased = false;

        if (ShotResetTimer < Time.time - ShotTimer)
        {
            ShotTimer = Time.time;
            AudioPlayer.Stop();
            AudioPlayer.Play();
            RaycastHit hit;
            Ray ray = new Ray(origin, direction + new Vector3(UnityEngine.Random.Range(-Spread, Spread), UnityEngine.Random.Range(-Spread, Spread), UnityEngine.Random.Range(-Spread, Spread)));
            //Debug.DrawRay(ray.origin, ray.direction * 50, Color.red, 1);

            if (Physics.Raycast(ray, out hit, Range))
            {
                //Debug.Log(hit.collider);
                if (hit.collider.tag == "Zombie")
                {
                    hit.transform.GetComponent<Zombie>().DealDamage(CalculateDamage(ParseBodypart(hit.collider.name)));
                }
            }
        }
    }

    public Bodypart ParseBodypart(string hitname)
    {
        if (hitname.Contains("Head"))
            return Bodypart.Head;
        else if (hitname.Contains("Spine"))
            return Bodypart.Chest;
        else if (hitname.Contains("UpperArm"))
            return Bodypart.UpperArm;
        else if (hitname.Contains("Forearm"))
            return Bodypart.LowerArm;
        else if (hitname.Contains("Hand"))
            return Bodypart.Hand;
        else if (hitname.Contains("Thigh"))
            return Bodypart.UpperLeg;
        else if (hitname.Contains("Calf"))
            return Bodypart.LowerLeg;
        else if (hitname.Contains("Foot")) 
            return Bodypart.Foot;
        else
            return Bodypart.Chest;
    }

    public virtual float CalculateDamage(Bodypart bodypart)
    {
        switch (bodypart)
        {
            case Bodypart.LowerArm:
                return Damage * 0.25f;
            case Bodypart.UpperArm:
                return Damage * 0.25f;
            case Bodypart.Hand:
                return Damage * 0.1f;

            case Bodypart.LowerLeg:
                return Damage * 0.25f;
            case Bodypart.UpperLeg:
                return Damage * 0.25f;
            case Bodypart.Foot:
                return Damage * 0.1f;

            case Bodypart.Chest:
                return Damage * 1f;
            case Bodypart.Head:
                return Damage * 2.5f;
            default:
                return Damage;
        }
    }
}
