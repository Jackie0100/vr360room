﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Camera360 : MonoBehaviour
{
    public float Aspect
    {
        get
        {
            return ((float)Screen.width / (float)Screen.height) / 4.0f;
        }
    }
    public float Aspect360
    {
        get
        {
            return (float)Screen.width / (float)Screen.height;
        }
    }

    public float HorizontalFieldOfView360
    {
        get
        {
            return GetComponentsInChildren<Camera>().Sum(c => Mathf.Atan(Mathf.Tan(c.fieldOfView * 0.5f) / Aspect) * 2.0f);
        }
    }

    public float VerticalFieldOfView360
    {
        get
        {
            return GetComponentsInChildren<Camera>().Sum(c => c.fieldOfView);
        }
    }

    public float HorizontalFieldOfView
    {
        get
        {
            return GetComponentsInChildren<Camera>().Sum(c => Mathf.Atan(Mathf.Tan(c.fieldOfView * 0.5f) / Aspect) * 2.0f) / 4.0f;
        }
    }

    public float VerticalFieldOfView
    {
        get
        {
            return GetComponentsInChildren<Camera>().Sum(c => c.fieldOfView) / 4.0f;
        }
    }

    void Start()
    {
        ResetFieldOfView();
    }

    void Update()
    {

    }

    void ResetFieldOfView()
    {
        foreach (Camera cam in GetComponentsInChildren<Camera>())
        {
            cam.fieldOfView = CalculateHorizontalFieldOfView(90);
        }
    }

    public float CalculateHorizontalFieldOfView(float fov)
    {
        return ((Mathf.Atan(Mathf.Tan((fov * Mathf.Deg2Rad) * 0.5f) / Aspect) * 2) * Mathf.Rad2Deg);
    }

    public Camera ScreenPositionToCamera(Vector3 pos)
    {
        return this.transform.GetChild(Mathf.FloorToInt(Mathf.Abs(pos.x / ((float)Screen.width / 4.0f))) % 4).GetComponent<Camera>();
    }
    public Camera ViewportPositionToCamera(Vector3 pos)
    {
        return this.transform.GetChild(Mathf.FloorToInt(Mathf.Abs(pos.x * 4)) % 4).GetComponent<Camera>();
    }

    public Camera WorldPositionToCamera(Vector3 pos)
    {
        Vector3 vec = new Vector3(((Mathf.Atan2(pos.y - this.transform.position.y, pos.z - this.transform.position.z) * Mathf.Rad2Deg) + 180.0f) % 360.0f,
            ((Mathf.Atan2(pos.x - this.transform.position.x, pos.z - this.transform.position.z) * Mathf.Rad2Deg) + 180.0f) % 360.0f,
            ((Mathf.Atan2(pos.x - this.transform.position.x, pos.y - this.transform.position.y) * Mathf.Rad2Deg) + 180.0f) % 360.0f);

        if (vec.y > 315 && vec.y <= 45)
        {
            return this.transform.GetChild(2).GetComponent<Camera>();
        }
        else if (vec.y > 45 && vec.y <= 135)
        {
            return this.transform.GetChild(0).GetComponent<Camera>();
        }
        else if (vec.y > 135 && vec.y <= 255)
        {
            return this.transform.GetChild(1).GetComponent<Camera>();
        }
        else if (vec.y > 255 && vec.y <= 315)
        {
            return this.transform.GetChild(2).GetComponent<Camera>();
        }
        return Camera.main;
    }

    public Vector2 LocalCameraScreenPositionToScreenPosition(Camera cam, Vector2 pos)
    {
        if (cam == this.transform.GetChild(0).GetComponent<Camera>())
        {
            return pos;
        }
        else if (cam == this.transform.GetChild(1).GetComponent<Camera>())
        {
            return new Vector2(((float)Screen.width / 4.0f) + pos.x, pos.y);
        }
        else if (cam == this.transform.GetChild(2).GetComponent<Camera>())
        {
            return new Vector2(((float)Screen.width / 4.0f) * 2 + pos.x, pos.y);
        }
        else if (cam == this.transform.GetChild(3).GetComponent<Camera>())
        {
            return new Vector2(((float)Screen.width / 4.0f) * 3 + pos.x, pos.y);
        }
        else
        {
            return Vector2.zero;
        }
    }

    public Vector3 LocalCameraScreenPositionToScreenPosition(Camera cam, Vector3 pos)
    {
        if (cam == this.transform.GetChild(0).GetComponent<Camera>())
        {
            return pos;
        }
        else if (cam == this.transform.GetChild(1).GetComponent<Camera>())
        {
            return new Vector3(((float)Screen.width / 4.0f) + pos.x, pos.y, pos.z);
        }
        else if (cam == this.transform.GetChild(2).GetComponent<Camera>())
        {
            return new Vector3(((float)Screen.width / 4.0f) * 2 + pos.x, pos.y, pos.z);
        }
        else if (cam == this.transform.GetChild(3).GetComponent<Camera>())
        {
            return new Vector3(((float)Screen.width / 4.0f) * 3 + pos.x, pos.y, pos.z);
        }
        else
        {
            return Vector3.zero;
        }
    }

    public Ray ScreenPointToRay(Vector3 position)
    {
        return ScreenPositionToCamera(position).ScreenPointToRay(position);
    }

    public Vector3 ScreenToViewportPoint(Vector3 position)
    {
        return ScreenPositionToCamera(position).ScreenToViewportPoint(position);
    }

    public Vector3 ScreenToWorldPoint(Vector3 position)
    {
        return ScreenPositionToCamera(position).ScreenToWorldPoint(position);
    }

    public Ray ViewportPointToRay(Vector3 position)
    {
        return ViewportPositionToCamera(position).ViewportPointToRay(position);
    }

    public Vector3 ViewportToScreenPoint(Vector3 position)
    {
        return ViewportPositionToCamera(position).ViewportToScreenPoint(position);
    }

    public Vector3 ViewportToWorldPoint(Vector3 position)
    {
        return ViewportPositionToCamera(position).ViewportToWorldPoint(position);
    }

    public Vector3 WorldToScreenPoint(Vector3 position)
    {
        return ScreenPositionToCamera(position).WorldToScreenPoint(position);
    }

    public Vector3 WorldToViewportPoint(Vector3 position)
    {
        return ScreenPositionToCamera(position).WorldToViewportPoint(position);
    }

    public Vector2 ScreenPositionToLocalCameraScreenPosition(Vector2 pos)
    {
        return new Vector2(pos.x % ((float)Screen.width / 4.0f), pos.y);
    }

    public Vector3 ScreenPositionToLocalCameraScreenPosition(Vector3 pos)
    {
        return new Vector3(pos.x % ((float)Screen.width / 4.0f), pos.y, pos.z);
    }

    Vector3 EulerAngles(Vector3 position)
    {
        return new Vector3(Mathf.Atan2(position.y - this.transform.position.y, position.z - this.transform.position.z),
            Mathf.Atan2(position.x - this.transform.position.x, position.z - this.transform.position.z),
            Mathf.Atan2(position.x - this.transform.position.x, position.y - this.transform.position.y)) * Mathf.Rad2Deg;
    }

    bool Approximate(float inputA, float inputB, float tolerance)
    {
        return Mathf.Abs(inputA - inputB) < tolerance;
    }
}
