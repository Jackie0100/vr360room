﻿using UnityEngine;

public class Gui360 : MonoBehaviour
{
    public float sizeX = 1;
    public float sizeY = 1;
    [SerializeField]
    bool width360 = true;
    [SerializeField]
    bool height360 = true;

    public GameObject spawnedHorizontalGameobject;
    public GameObject spawnedVerticalGameobject;


    bool isHorizontalGameObjectSpawned {  get { return spawnedHorizontalGameobject != null; } }
    bool isVerticalGameObjectSpawned { get { return spawnedHorizontalGameobject != null; } }


    void Start()
    {
        sizeX = (this.GetComponent<RectTransform>().anchorMax.x - this.GetComponent<RectTransform>().anchorMin.x) * GetCanvasWidth();
        sizeY = (this.GetComponent<RectTransform>().anchorMax.y - this.GetComponent<RectTransform>().anchorMin.y) * GetCanvasHeight();

    }

    // Update is called once per frame
    void Update ()
    {
        if (width360)
        {
            if (!isHorizontalGameObjectSpawned && this.GetComponent<RectTransform>().position.x < 0 + (sizeX / 2.0f))
            {
                SpawnObject(new Vector3(this.transform.position.x + GetCanvasWidth(), this.transform.position.y, this.transform.position.z));
            }
            else if (!isHorizontalGameObjectSpawned && this.GetComponent<RectTransform>().position.x > GetCanvasWidth() - (sizeX / 2.0f))
            {
                SpawnObject(new Vector3(this.transform.position.x - GetCanvasWidth(), this.transform.position.y, this.transform.position.z));
            }

            if (this.GetComponent<RectTransform>().position.x < -(sizeX / 2.0f) && isHorizontalGameObjectSpawned)
            {
                GameObject.Destroy(this.gameObject);
            }
            else if (this.GetComponent<RectTransform>().position.x > (GetCanvasWidth() + (sizeX / 2.0f)) && isHorizontalGameObjectSpawned)
            {
                GameObject.Destroy(this.gameObject);
            }
        }
        if (height360)
        {
            //TODO: Add handles to extra screens
            /*
            if (!isVerticalGameObjectSpawned && this.GetComponent<RectTransform>().position.y < 0 + (sizeY / 2.0f))
            {
                SpawnObject(new Vector3(this.transform.position.x + GetCanvasWidth(), this.transform.position.y, this.transform.position.z));
            }
            else if (!isVerticalGameObjectSpawned && this.GetComponent<RectTransform>().position.y > GetCanvasHeight() - (sizeY / 2.0f))
            {
                SpawnObject(new Vector3(this.transform.position.x - GetCanvasWidth(), this.transform.position.y, this.transform.position.z));
            }

            if (this.GetComponent<RectTransform>().position.y < -(sizeY / 2.0f) && isVerticalGameObjectSpawned)
            {
                GameObject.Destroy(this.gameObject);
            }
            else if (this.GetComponent<RectTransform>().position.x > (GetCanvasHeight() + (sizeY / 2.0f)) && isVerticalGameObjectSpawned)
            {
                GameObject.Destroy(this.gameObject);
            }*/
        }
    }

    public float GetCanvasHeight()
    {
        return this.transform.root.GetComponent<RectTransform>().sizeDelta.y;
    }

    public float GetCanvasWidth()
    {
        return this.transform.root.GetComponent<RectTransform>().sizeDelta.x;
    }

    void SpawnObject(Vector3 pos)
    {
        GameObject go = GameObject.Instantiate(this.gameObject);
        go.transform.SetParent(this.transform.parent, false);
        go.GetComponent<Gui360>().spawnedHorizontalGameobject = this.gameObject;
        go.transform.position = pos;
        go.name = this.name;
        spawnedHorizontalGameobject = go;
    }
}