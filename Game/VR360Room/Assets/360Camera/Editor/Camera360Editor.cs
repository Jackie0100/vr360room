﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Camera360), true)]
public class Camera360Editor : Editor
{
    Camera360 camera;

    float nearClipPlane = 0.1f;
    float farClipPlane = 1000f;

    bool yneg;
    bool ypos;

    float numbersOfCameras
    {
        get
        {
            if (yneg && ypos)
            {
                return 6;
            }
            else if (yneg ^ ypos)
            {
                return 5;
            }
            else
            {
                return 4;
            }
        }
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        camera = (Camera360)target;

        nearClipPlane = EditorGUILayout.FloatField("Near Clipping Plane", nearClipPlane);
        farClipPlane = EditorGUILayout.FloatField("Far Clipping Plane", farClipPlane);


        yneg = GUILayout.Toggle(yneg, "Camera Y Negative");
        ypos = GUILayout.Toggle(ypos, "Camera Y Positive");

        //Debug.Log(Handles.GetMainGameViewSize());

        if (GUILayout.Button("Apply"))
        {
            SetupCameras();
        }

    }

    void SetupCameras()
    {
        for (int i = 0; i < camera.transform.childCount; )
        {
            GameObject.DestroyImmediate(camera.transform.GetChild(i).gameObject);
        }

        CreateCameraComponent("CameraX-", Quaternion.Euler(0, -90, 0), 0);
        CreateCameraComponent("CameraZ+", Quaternion.Euler(0, 0, 0), 1);
        CreateCameraComponent("CameraX+", Quaternion.Euler(0, 90, 0), 2);
        CreateCameraComponent("CameraZ-", Quaternion.Euler(0, 180, 0), 3);

        if (yneg && !ypos)
        {
            CreateCameraComponent("CameraY-", Quaternion.Euler(-90, 0, 0), 4);
        }
        else if (ypos && !yneg)
        {
            CreateCameraComponent("CameraY+", Quaternion.Euler(90, 0, 0), 4);
        }
        else if (yneg && ypos)
        {
            CreateCameraComponent("CameraY-", Quaternion.Euler(-90, 0, 0), 4, true);
            CreateCameraComponent("CameraY+", Quaternion.Euler(90, 0, 0), 5, true);
        }
    }

    void CreateCameraComponent(string name, Quaternion rotation, float cameranumber, bool isYcam = false)
    {
        GameObject go = new GameObject();
        go.name = name;
        go.transform.SetParent(camera.transform, false);
        go.transform.rotation = rotation;
        Camera cam = go.AddComponent<Camera>();
        go.AddComponent<GUILayer>();
        go.AddComponent<FlareLayer>();

        if (isYcam)
        {
            cam.rect = new Rect(((1 / numbersOfCameras) * cameranumber) + ((Handles.GetMainGameViewSize().y / Handles.GetMainGameViewSize().x) / 2.0f), 0, (Handles.GetMainGameViewSize().y / Handles.GetMainGameViewSize().x), 1);
            cam.nearClipPlane = nearClipPlane;
            cam.farClipPlane = (Handles.GetMainGameViewSize().y / (Handles.GetMainGameViewSize().x / numbersOfCameras)) * farClipPlane;
            cam.fieldOfView = 180 - (CalculateHorizontalFieldOfView(90));
        }
        else
        {
            cam.rect = new Rect((1 / numbersOfCameras) * cameranumber, 0, (1 / numbersOfCameras), 1);
            cam.nearClipPlane = nearClipPlane;
            cam.farClipPlane = farClipPlane;
            cam.fieldOfView = CalculateHorizontalFieldOfView(90);
        }
    }

    float CalculateHorizontalFieldOfView(float fov)
    {
        return ((Mathf.Atan(Mathf.Tan((fov * Mathf.Deg2Rad) * 0.5f) / (Handles.GetMainGameViewSize().x / numbersOfCameras / Handles.GetMainGameViewSize().y)) * 2) * Mathf.Rad2Deg);
    }
}
