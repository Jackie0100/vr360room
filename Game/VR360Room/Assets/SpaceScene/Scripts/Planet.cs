﻿using UnityEngine;
using System.Collections;

public class Planet : MonoBehaviour
{
    [SerializeField]
    Vector3 rot;
	
	// Update is called once per frame
	void Update ()
    {
        this.transform.Rotate(rot * Time.deltaTime);
	}
}
