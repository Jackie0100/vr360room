﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour
{
    [SerializeField]
    Vector3 position;
    [SerializeField]
    Vector3 direction;
    [SerializeField]
    float speed;
	
	// Update is called once per frame
	void Update ()
    {
        this.transform.RotateAround(position, direction, speed * Time.deltaTime);
	}
}
