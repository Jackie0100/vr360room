﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class MusicStarter : MonoBehaviour
{
    GameObject Camera360;

    void Start()
    {
        Camera360 = GameObject.Find("360Camera");
    }

	void Update ()
    {
        if (Vector3.Distance(this.transform.position, Camera360.transform.position) < 2 && !this.GetComponent<AudioSource>().isPlaying)
        {
            this.GetComponent<AudioSource>().Play();
        }
	}
}
