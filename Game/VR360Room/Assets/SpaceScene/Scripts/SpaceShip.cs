﻿using UnityEngine;
using System.Collections;

public class SpaceShip : MonoBehaviour
{
    [SerializeField]
    AudioClip[] laserSounds;
    [SerializeField]
    GameObject enemyShip;
    [SerializeField]
    GameObject laser;
    [SerializeField]
    GameObject explosion;
    [SerializeField]
    bool explode;
    [SerializeField]
    bool doSpin;
    public bool exploded;
    AudioSource laserSource1;
    AudioSource laserSource2;


    float timer;

    void Start()
    {
        laserSource1 = this.gameObject.AddComponent<AudioSource>();
        laserSource1.playOnAwake = false;
        laserSource1.spatialBlend = 1;
        laserSource1.minDistance = 1;
        laserSource1.maxDistance = 10;
        laserSource2 = this.gameObject.AddComponent<AudioSource>();
        laserSource2.playOnAwake = false;
        laserSource2.spatialBlend = 1;
        laserSource2.minDistance = 1;
        laserSource2.maxDistance = 10;
    }

    // Update is called once per frame
    void Update ()
    {
        if (enemyShip == null || doSpin && Vector3.Distance(this.transform.position, enemyShip.transform.position) < 30)
        {
            this.transform.Rotate(Vector3.forward * Time.deltaTime * 70);
        }
        if (enemyShip == null)
            return;

        timer += Time.deltaTime;
	    if (timer >= 0.5f && !enemyShip.GetComponent<SpaceShip>().exploded && Vector3.Distance(this.transform.position, enemyShip.transform.position) < 30)
        {
            if (Vector3.Distance(this.transform.position, enemyShip.transform.position) < 5 && explode)
            {
                exploded = true;
                GameObject.Instantiate(explosion, this.transform.position, Quaternion.identity);
                Destroy(this.gameObject);
            }
            timer = 0;
            GameObject.Instantiate(laser, this.transform.position + new Vector3(0.155f, 0, 0.35f), this.transform.rotation);
            GameObject.Instantiate(laser, this.transform.position + new Vector3(-0.155f, 0, 0.35f), this.transform.rotation);
            laserSource1.clip = laserSounds[Random.Range(0, laserSounds.Length)];
            laserSource2.clip = laserSounds[Random.Range(0, laserSounds.Length)];
            laserSource1.Play();
            laserSource2.Play();
        }
    }
}
