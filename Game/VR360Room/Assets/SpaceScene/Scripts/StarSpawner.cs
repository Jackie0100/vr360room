﻿using UnityEngine;
using System.Collections;

public class StarSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject[] stars;

	// Use this for initialization
	void Start ()
    {
        Random.seed = 1;
        for (int i = 0; i < 1000; i++)
        {
            GameObject go = (GameObject)GameObject.Instantiate(stars[Random.Range(0, stars.Length)], (Random.insideUnitSphere * 10) * 7, Quaternion.identity);

            go.transform.parent = this.transform;
        }
    }
}
