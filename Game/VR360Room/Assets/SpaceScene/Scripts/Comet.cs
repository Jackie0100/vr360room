﻿using UnityEngine;
using System.Collections;

public class Comet : MonoBehaviour
{
    [SerializeField]
    Vector3 direction;
    [SerializeField]
    float speed;

	// Update is called once per frame
	void Update ()
    {
        transform.Translate(direction.normalized * speed * Time.deltaTime);
	}

    void OnParticleCollision(GameObject other)
    {

    }
}
